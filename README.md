# README #

This is the second project for the Cmpe 436 : Concurrent & Distributed Programming course

## General information

* To run the gameOfLife program you must supply minimum of 3 arguments. M, N and maximum number of generation. If you add population file, the program automatically imports the population and checks whether the M and N values you supplied match the ones in the input file. You can see the run examples below 
``` bash 

java gameOfLife 5 5 25 

java gameOfLife 5 5 25 input.txt

```

* To run the DeadLockExample program, you just need to run the program itself. The deadlock scenerio is a mexican standoff where each cowboy draws his gun and program halts. 
``` bash 

java DeadLockExample

```
* To run the RaceConditionExample program, you just need to run the program itself. The program contains a scenerio with one race condition and one without. The second threads always shows 101-212-323-434-545 values while the first thread loop results varies upon execution.
``` bash 

java RaceConditiomExample

```
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;
import java.util.StringJoiner;

/**
 * Created by yigitozgumus on 10/7/16.
 */
public class GameOfLife {

    private Integer[][] grid;
    private Integer[][] currentPopulation;
    private Integer M;
    private Integer N;
    private Integer maxGen;
    private Integer threadNumber;
    private GoLAgent[] threads;
    Integer countOfThreads = 0;


    BinarySemaphore mutex ;
    CountingSemaphore barrier ;
    CountingSemaphore barrier2;


    class GoLAgent extends Thread {
        int id;
       private int row;
       private int col;
       private int updateIndividual =0;


        public GoLAgent(int id) {
            this.id = id;
            row = (int)Math.floor(1.0 * id/N);
            col = id % N;
        }

        public void run() {

            //Thread execution
            try {
                for (int i = 0; i < maxGen; i++){
                    try {
                        mutex.P();
                    } catch (InterruptedException e) {
                         e.printStackTrace();
                    }
                    countOfThreads += 1;
                    if (countOfThreads == threadNumber) {
                        try {
                            barrier2.P();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        barrier.V();
                    }
                    mutex.V();
                    try {
                        barrier.P();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    barrier.V();
                    updateIndividual = checkNeighbors(grid,row, col);
                   // System.out.println(String.valueOf(id) + "--" + String.valueOf(updateIndividual));
                    try {
                        mutex.P();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    countOfThreads -= 1;
                    if (countOfThreads == 0) {
                        System.out.println("who am i? --" + String.valueOf(id));

                        try {
                            barrier.P();

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        barrier2.V();

                    }
                    mutex.V();

                    try {
                        barrier2.P();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    grid[row][col] = updateIndividual;
                    barrier2.V();

                }
            }catch(Exception e){
            }

        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        GameOfLife GoL = new GameOfLife();
        GoL.initialize(args);
        GoL.createThreads();
        GoL.startThreads();
        System.out.println("State of the population in the "+GoL.maxGen+". generation:\n");
        GoL.printPopulation(GoL.grid);

    }

    public void initialize(String[] args) throws IOException {
        M = Integer.parseInt(args[0]);
        N = Integer.parseInt(args[1]);
        threadNumber = M * N ;
        threads = new GoLAgent[threadNumber];
        mutex = new BinarySemaphore(true);
        barrier = new CountingSemaphore(0);
        barrier2 = new CountingSemaphore(1);
        maxGen = Integer.parseInt(args[2]);
        grid = new Integer[M][N];
        currentPopulation = new Integer[M][N];
        if (args.length == 3){
            generatePopulation();
        }else{
            importPopulation(args[3]);
        }
    }
    //This method creates the threads
    public void createThreads(){
        for (int i = 0; i < threadNumber ; i++) {
            threads[i] = new GoLAgent(i);
        }
    }
    //This method starts threads
    public void startThreads() throws InterruptedException {

            for (int i = 0; i < threadNumber ; i++) {
                if (threads[i].getState() == Thread.State.NEW) {
                    threads[i].start();
                }
            }
            for (int i = 0; i <threadNumber ; i++) {
                threads[i].join();
            }

    }
    // Import population method, just like in first project
    public void importPopulation(String fileName) throws IOException {
        try(BufferedReader br = new BufferedReader(new FileReader(fileName))){
            StringBuilder sb = new StringBuilder();
            //Get the first line
            String line = br.readLine();
            String[] larray = line.split("\\s+");
            Integer NSub = larray.length;
            Integer MSub = 0;
            if (NSub != N){
                throw new IllegalArgumentException("N and columns of input.txt does not match.");
            }
            while(line != null){
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
                MSub +=1;
            }
            if (MSub != M){
                throw new IllegalArgumentException("M and rows of input.txt does not match.");
            }
            String allGrid = sb.toString();
            allGrid.replace("\n"," ");
            String[] tempGrids = allGrid.split("\\s+");
            for (int i = 0; i < M ; i++) {
                for (int j = 0; j < N; j++) {
                    grid[i][j] = Integer.parseInt(tempGrids[i * N + j]);
                }
            }
        }
    }

    public void generatePopulation(){
        Random rand = new Random(112358);
        for (int i = 0; i < M ; i++) {
            for (int j = 0; j < N; j++) {
                grid[i][j] = rand.nextInt(2);
            }
        }
    }

    public int checkNeighbors(Integer[][] target,int row,int col){
        int rowMaxLimit = target.length-1;
        int colMaxLimit = target[0].length-1;
        int rowMinLimit = 0;
        int colMinLimit = 0;
        int count = 0;
        int rowStart = Math.max(rowMinLimit,row-1);
        int rowEnd = Math.min(rowMaxLimit,row+1);
        int colStart = Math.max(colMinLimit,col-1);
        int colEnd = Math.min(colMaxLimit,col+1);

        for (int i = rowStart; i <=rowEnd ; i++) {
            for (int j = colStart; j <= colEnd ; j++) {
                count += target[i][j];
            }
        }
        count -= target[row][col];
        if (target[row][col] == 1){
            if (count == 2 || count == 3) {return 1;}else{return 0;}
        }else{
            if(count ==3){return 1;}else{return 0;}
        }
    }


    public void printPopulation(Integer[][] target){
        for (int i = 0; i < M ; i++) {
            for (int j = 0; j < N ; j++) {
                System.out.print(target[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
    // For debug
    public void outputResult() throws FileNotFoundException, UnsupportedEncodingException {
        String outputFileName = "GameOfLife2";
        Path outputFile = Paths.get(outputFileName);
        String delimiter = System.getProperty("line.separator");
        StringJoiner sb = new StringJoiner(delimiter);
        for (Integer[] row : grid)
            sb.add(Arrays.toString(row));
        String s = sb.toString().replaceAll("\\[|\\]|\\,","");
        PrintWriter pw = new PrintWriter(outputFileName.toString(),"UTF-8");
        pw.print(s);
        pw.close();
        System.out.println("The result is written to the " + outputFileName + ".");

    }


}

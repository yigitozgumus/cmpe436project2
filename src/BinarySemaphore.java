/**
 * Created by yigitozgumus on 10/19/16.
 */

class BinarySemaphore {
    boolean value;
    BinarySemaphore(boolean initValue){
        this.value = initValue;
    }
    public synchronized void P() throws InterruptedException {
        while(value == false)
            wait();
        value = false;
    }
    public synchronized void V(){
        value = true;
        notifyAll();
    }

}

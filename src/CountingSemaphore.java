/**
 * Created by yigitozgumus on 10/19/16.
 */
class CountingSemaphore {
    int value;
    public CountingSemaphore(int initValue)
    {
        this.value = initValue;
    }
    public synchronized void P() throws InterruptedException {

        while(value <= 0){
            this.wait();
        }
        value--;

    }
    public synchronized void V(){
        value++;
        this.notifyAll();

    }
}

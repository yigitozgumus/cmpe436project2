/**
 * Created by yigitozgumus on 10/20/16.
 */
public class CountingSemaphoreV2 {
    int value ;
    BinarySemaphore wait;
    int wCount;
    BinarySemaphore mutex;

    public CountingSemaphoreV2(int initValue){
        this.value = initValue;
        wait = new BinarySemaphore(false);
        wCount = 0;
        mutex = new BinarySemaphore(true);
    }

    public synchronized void P() throws InterruptedException {
        mutex.P();
        if (value ==0){
            wCount += 1;
            mutex.V();
            wait.P();
        }else{
            value -= 1;
            mutex.V();
        }
    }
    public synchronized void V() throws InterruptedException {
        mutex.P();
        if(wCount > 0){
            wCount -= 1;
            wait.V();
            mutex.P();
        }else{
            value += 1;
            mutex.V();
        }
    }
}

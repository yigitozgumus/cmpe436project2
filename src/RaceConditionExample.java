/**
 * Created by yigitozgumus on 10/20/16.
 */
public class RaceConditionExample {

    int globalCounter = 0;

    public static void main(String[] args) throws InterruptedException {
        System.out.println("First Scenerio with race condition.");
        RaceConditionExample test = new RaceConditionExample();
        for (int i = 0; i <4 ; i++) {
            test.createThreadsWithRace();
            test.globalCounter = 0;

        }
        System.out.println("Second Scenerio without a race condition");
        RaceConditionExample test2 = new RaceConditionExample();
        for (int i = 0; i < 4 ; i++) {
            test2.createThreadsWithoutRace();
        }
    }
    public void createThreadsWithRace() throws InterruptedException {
        Thread t1 = new Thread(() -> {
            incrementCounter();
            System.out.println("Thread 1 counter: " + String.valueOf(globalCounter));
        });
        Thread t2 = new Thread(() -> {
            decrementCounter();
            System.out.println("Thread 2 counter: " + String.valueOf(globalCounter));
        });
        Thread t3 = new Thread(() -> {
            incrementCounter();
            System.out.println("Thread 3 counter: " + String.valueOf(globalCounter));
        });
        t1.start();
        t2.start();
        t3.start();

    }
    public void createThreadsWithoutRace() throws InterruptedException {
        Thread t1 = new Thread(() -> {
            synchronized (this){
            incrementCounter();
            }
            System.out.println("Thread 1 counter: " + String.valueOf(globalCounter));
        });
        Thread t2 = new Thread(() -> {
            synchronized (this){
                decrementCounter();
            }
            System.out.println("Thread 2 counter: " + String.valueOf(globalCounter));
        });
        Thread t3 = new Thread(() -> {
            synchronized (this){
                incrementCounter();
            }
            System.out.println("Thread 3 counter: " + String.valueOf(globalCounter));
        });
        t1.start();
        t2.start();
        t3.start();
        t1.join();
        t2.join();
        t3.join();
    }

    public void incrementCounter(){
        globalCounter++;
    }
    public void decrementCounter(){
        globalCounter--;
    }
}

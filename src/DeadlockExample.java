/**
 * Created by yigitozgumus on 10/20/16.
 */
public class DeadlockExample {

    static Object theGood  = new Object();
    static Object theBad  = new Object();
    static Object theUgly  = new Object();

    public static void main(String[] args) {
        DeadlockExample typicalWesternMovie = new DeadlockExample();
        typicalWesternMovie.mexicanStandOff();
    }

    public void mexicanStandOff(){
        Thread theGood = new Thread(() -> {
            theGoodShoots();
        });
        Thread theBad = new Thread(() -> {
            theBadShoots();
        });
        Thread theUgly = new Thread(() -> {
            theUglyShoots();
        });
        theGood.start();
        // ,
        theBad.start();
        // and the
        theUgly.start();
    }

    public void theGoodShoots(){
        synchronized (theGood){
            System.out.println("The good is reaching for his gun");
            takeOutGun();
            synchronized (theUgly){
                System.out.println("the good is shooting the ugly.");
                shoot();
                synchronized (theBad) {
                    System.out.println("the good is shooting the bad.");
                    shoot();
                }
                System.out.println("The ugly is killed by the good.");
            }
            System.out.println("the bad is killed by the good.");
        }
    }
    public void theBadShoots(){
        synchronized (theBad){
            System.out.println("The bad is reaching for his gun");
            takeOutGun();
            synchronized (theUgly) {
                System.out.println("the bad is shooting the ugly.");
                shoot();

                synchronized (theGood) {
                    System.out.println("the bad is shooting the good.");
                    shoot();
                }
                System.out.println("The good is killed by the bad.");
            }
            System.out.println("the ugly is killed by the bad.");
        }}
    public void theUglyShoots(){
        synchronized (theUgly){
            System.out.println("The ugly is reaching for his gun");
            takeOutGun();
            synchronized (theGood){
                System.out.println("the ugly is shooting the good.");
                shoot();

                synchronized (theBad) {
                    System.out.println("the ugly is shooting the bad.");
                    shoot();
                }
                System.out.println("The good is killed by the ugly.");
            }
            System.out.println("the bad is killed by the ugly.");
        }}

    private void shoot() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private void takeOutGun(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
